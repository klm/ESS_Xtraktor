unit u_noname;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }
  TIntegerArray = array of integer;

  TForm1 = class(TForm)
    Button2: TButton;
    Button3: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //function BSwap16(I: WORD): WORD;


  private

  public
    { public declarations }
  end;

var
  USAGE: string = 'Aucun arguments trouvé. USAGE: fichier (separator):String (debuglevel):int (stripvalues):bool';
  Form1: TForm1;
  FICHIER: string;
  SEPARATOR: string = ';';
  OutputFile: Text;
  DataList: TStringList;
  DataValues: TStringList;
  ASCIIValues: TStringList;
  NOSTRIP: boolean = False;
  DEBUG: integer = 0;
  CUSTOMEROFFSET: integer = -2;
  SERIALOFFSET: integer = 0;
  DEVNAMEOFFSET: integer = 1;
  PRODNAMEOFFSET: integer = 2;
  MAILOFFSET: integer = 83;
  PUREMAILOFFSET: integer = 93;
  DEVADMOFFSET: integer = 7;
  DEVICEADMINIDENTIFIER: string = 'DeviceAdmin';
  CESSDATACOMBOIDENTIFIER: string = 'CESSDataCombo';
  EMBEDEDRDSIDENTIFIER: string = 'CEmbeddedRDSlF';
  DEVICESTARTIDENTIFIER: string = 'AvailableG';
  CSTOCKLOCATIONIDENTIFIER: string = 'CStockLocation';
  SEPARATORVALUESIDENTIFIER: string = 'FF';
  SEPARATORVALUESIDENTIFIER2: string = 'FE';
  retourdestrip: string = '';
  EMAILHEXMARKER: string = 'FEFF001980A20000006008000000010108000000FFFE';
  mailadr: TStringList;

function DevicesIndexe(DataList: TStringList): TIntegerArray;
function HexToString(H: string): string;
procedure writelntabledebug(Datalist: TStringList);
procedure writedebug(chaine: string);
procedure writedebug1(chaine: string);
procedure writedebug2(chaine: string);
procedure writedebug(chaine: integer);
procedure writedebug1(chaine: integer);
procedure writedebug2(chaine: integer);
function DeviceDataExtract(DeviceStartList: TIntegerArray; TableDeTravail: TStringList): TStringList;
function LignesVidesStrip(DataList: TStringList): TStringList;
function LigneConstructFull(DevicesInfos: TStringList; CustomerInfo: string; MailInfo: TStringList; DeviceNumber: integer): TStringList;
function CustomerExtract(DataList: TStringList): string;
function striplastchar(Chaine: string): string;
function DeviceMailExtract(Datas: TStringList; DeviceIndex: integer; DeviceNumber: integer): TStringList;
function DeviceMailExtractBAD(DataList: TStringList; DeviceIndex: integer): string;
function StripNonAscii(chaine: string): string;
function StripNonAsciiAndShowStripped(chaine: string; return: string): string;
function StripNonAsciiAndShowStrippedOnHex(chaine: string; return: string): string;
function IsChar(c: char): boolean;
function StringToHex(S: string): string;

implementation

  {$R *.lfm}

{ TForm1 }
function StripNonAscii(chaine: string): string;
var
  charactere: char;
  chainestrip: string = '';
begin
  for charactere in chaine do
  begin
    if IsChar(charactere) then
      chainestrip := chainestrip + charactere;
  end;
  Result := chainestrip;
end;

function StripNonAsciiAndShowStrippedOnHex(chaine: string; return: string): string;
var
  charactere: char;
  chainestrip: string = '';
  stripped: string = '';
begin
  for charactere in chaine do
  begin
    if IsChar(charactere) then
    begin
      chainestrip := chainestrip + charactere;
      stripped := stripped + ' ';
    end
    else
    begin
      stripped := stripped + byte(charactere).ToHexString;
      //return:= return+StringToHex(charactere);
    end;
  end;
  writedebug(stripped);
  Result := chainestrip;
end;

function StripNonAsciiAndShowStripped(chaine: string; return: string): string;
var
  charactere: char;
  chainestrip: string = '';
begin
  for charactere in chaine do
  begin
    if IsChar(charactere) then
    begin
      chainestrip := chainestrip + charactere;
    end
    else
    begin
      return := return + charactere;
    end;
  end;
  Result := chainestrip;
end;

function HexToString(H: string): string;
var
  I: integer;
begin
  Result := '';
  try
    for I := 1 to length(H) div 2 do
      Result := Result + char(StrToInt('$' + Copy(H, (I - 1) * 2 + 1, 2)));
  except
    on E: Exception do
    begin
      Writeln('HexToString: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;
end;

function StringToHex(S: string): string;
var
  I: integer;
begin
  Result := '';
  try
    for I := 1 to length(S) do
      Result := Result + IntToHex(Ord(S[i]), 2);
  except
    on E: Exception do
    begin
      Writeln('StringToHex: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;

end;

function IsChar(c: char): boolean;
var
  s: boolean;
begin
  s := False;
  case c of
    '0' .. '9': s := True;
    'a' .. 'z': s := True;
    'A' .. 'Z': s := True;
    '+', '-', '@', '.', ' ': s := True;
  end;
  Result := s;
end;

function LignesVidesStrip(DataList: TStringList): TStringList;
var
  DataListFiltered: TStringList;
  DataLigne: string;
begin
  // Filtre des lignes vides (cette étape est parfaitement inutile...
  // elle ne me sert que pour mon reverse engeneering)
  try
    DataListFiltered := TStringList.Create;
    for DataLigne in DataList do
    begin
      if (DataLigne.Length <> 0) and (DataLigne <> ' ') then
      begin
        DataListFiltered.Append(DataLigne);
      end;
    end;
    writedebug1('Lignes non vides: ' + DataList.Count.ToString);
    Result := DataListFiltered;
  except
    on E: Exception do
    begin
      Writeln('LignesVidesStrip: Une erreur de liste s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  DataLines: string;
  counter: integer = 0;
begin
  ASCIIValues := TStringList.Create;
  for DataLines in DataValues do
  begin
    writedebug(counter.ToString + ' - "' + StripNonAscii(HexToString(DataLines)) + '"');
    counter := counter + 1;
    ASCIIValues.Append(StripNonAscii(HexToString(DataLines)));
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  DeviceUnitIndex: integer;
  DeviceNumber: integer = 0;
  DeviceList: TIntegerArray;
  LIGNE: string;
  DevicesListFull: TStringList;
  TableDeTravail: TStringList;
begin
  try
    TableDeTravail := ASCIIValues;
    DeviceList := DevicesIndexe(TableDeTravail);
    writedebug1('Button3Click - Longueure de  DeviceList:' + Length(DeviceList).ToString);
    DevicesListFull := TStringList.Create;
    try
      DeviceNumber := 0;
      mailadr := TStringList.Create;
      for DeviceUnitIndex in DeviceList do
      begin
        DevicesListFull := LigneConstructFull(DeviceDataExtract(DeviceList, ASCIIValues), CustomerExtract(
          TableDeTravail), DeviceMailExtract(DataValues, DeviceUnitIndex, DeviceNumber), DeviceNumber);
        writedebug('Button3Click - DeviceUnitIndex: ' + DeviceUnitIndex.ToString + ' - Device number: ' + DeviceNumber.ToString);
        DeviceNumber := DeviceNumber + 1;
      end;
    except
      on E: Exception do
      begin
        Writeln('Button3Click: Erreur a la creation de DevicesListFull. Détails: ' + E.ClassName + '/' + E.Message);
      end;
    end;

    try
      for LIGNE in DevicesListFull do
      begin
        writeln(LIGNE);
      end;
    except
      on E: Exception do
      begin
        Writeln('Button3Click: Erreur a la lecture de DevicesListFull. Détails: ' + E.ClassName + '/' + E.Message);
      end;
    end;
  except
    on E: Exception do
    begin
      Writeln('Button3Click: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;
end;

//################################################################################################################
//Extraction des données voulues
//  3 = Customer Name
// la ça se complique, on as une longueure variable de données selon le nombre de devices.
// il faut d'abord rechercher la déclaration des devices dans le fichier pour les compter.
// on match sur CEmbeddedRDS pour la position de départ. On doit extraire les numeros de serie
// qui serviront a reperer la fin de la declaration du device et a partir sur un nouveau device.
// ensuite pour trouver les valeurs voulues, il faut prendre, pour chaque position de device
// +2 = Device Name
// +4 = Product Name
function DevicesIndexe(DataList: TStringList): TIntegerArray;
var
  countj: integer;
  openedDevice: boolean;
  offset: integer;
  DataLigne: string;
  lastStopDevice: integer;
  DeviceCountAndPositions: TIntegerArray;
begin
  try
    offset := 1;
    openedDevice := False;
    SetLength(DeviceCountAndPositions, 0);
    countj := 0;
    // on parcours notre tableau de lignes
    for DataLigne in DataList do
    begin

      // Et on y recherche les déclarations de Devices avec le mot clef AvailableG
      // Methode par mot clef AvailableG qui semble être présent avant chaque début de Device et utilisé nulle
      // part ailleurs. semble fiable sur les 4 essais faits.
      writedebug1('Testing value : ' + DataLigne);
      if DataLigne = DEVICESTARTIDENTIFIER then
      begin
        lastStopDevice := countj;
        openedDevice := True;
        writedebug1('Device found! : ' + DataLigne);
      end;

      // Si on ne dépasse pas la capacité de notre tableau en travaillant a +offset en aval, alors on peut y aller:
      if countj + offset < DataList.Count then
      begin
        // Si notre position actuelle correspond a la dernière position connue d'un debut de Device
        // et qu'un device viens d'être ouvert
        if (countj = lastStopDevice) and (openedDevice = True) then
        begin
          // alors on indexe sa position dans notre liste.
          SetLength(DeviceCountAndPositions, Length(DeviceCountAndPositions) + 1);
          DeviceCountAndPositions[length(DeviceCountAndPositions) - 1] := countj + offset;
          // Et on indique que l'on referme notre device pour pouvoir passer au suivant sans indexer par erreur
          // toutes nos lignes a suivre.
          openedDevice := False;
        end;
      end;
      countj := countj + 1;
    end;
  except
    on E: Exception do
    begin
      Writeln('Une erreur de liste s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;
  Result := DeviceCountAndPositions;
end;

function LigneConstructFull(DevicesInfos: TStringList; CustomerInfo: string; MailInfo: TStringList; DeviceNumber: integer): TStringList;
var
  ResultFull: TStringList;
  DataLigne: string;
  SEP: string;
  DeviceCounter: integer = 0;
begin
  writedebug('LigneConstructFull - Args: ' + DevicesInfos.ToString + ' - ' + CustomerInfo + ' - ' + MailInfo.ToString +
    ' - ' + DeviceNumber.ToString);
  try
    SEP := SEPARATOR;
    ResultFull := TStringList.Create;

    for DataLigne in DevicesInfos do
    begin

      try
        if MailInfo.Count > DeviceCounter then
        begin
          ResultFull.Append(CustomerInfo + SEP + DataLigne + SEP + striplastchar(MailInfo[DeviceCounter]));
          writedebug('LigneConstructFull - MailInfo[DeviceNumber]: ' + MailInfo[DeviceNumber] + '( ' + DeviceNumber.ToString + ' )');
          DeviceCounter := DeviceCounter + 1;

        end;

      except
        on E: Exception do
        begin
          Writeln('LigneConstructFull: Une erreur d''index de tableau produite. Détails: ' + E.ClassName + '/' + E.Message);
        end;
      end;

    end;
    Result := ResultFull;
  except
    on E: Exception do
    begin
      Writeln('LigneConstructFull: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;

end;

function striplastchar(Chaine: string): string;
begin
  if not NOSTRIP then
    Result := copy(chaine, 0, chaine.Length - 1)
  else
    Result := chaine;
end;

function CustomerExtract(DataList: TStringList): string;
var
  DataLigneCE: string;
  counti: integer = 0;
  customername: string = '';
begin
  for DataLigneCE in DataList do
  begin
    writedebug2(counti.tostring + ' - ' + CUSTOMEROFFSET.ToString);
    if ((DataLigneCE = CESSDATACOMBOIDENTIFIER) and (counti >= 0 + CUSTOMEROFFSET)) then
    begin
      customername := DataList[counti + CUSTOMEROFFSET];
      writedebug1('CustomerExtract - DataLigneCE=' + DataLigneCE);
    end;
    counti := counti + 1;
  end;

  if DEBUG >= 1 then
  begin
    for DataLigneCE in DataList do
    begin
      if (DataLigneCE = CESSDATACOMBOIDENTIFIER) then
        writedebug1('CustomerExtract - DataLigneCE=' + DataLigneCE);
    end;
  end;

  Result := customername;
end;

function DeviceMailExtract(Datas: TStringList; DeviceIndex: integer; DeviceNumber: integer): TStringList;
var
  //Compteur de lignes de données
  counter: integer = 0;
  //Compteur de données par devices
  counti: integer = 0;
  ligne: string;
  DeviceLignes: string;
  chainedebug: string = '';
begin
  writedebug('DeviceMailExtract - Args: ' + Datas.ToString + ' - ' + DeviceIndex.ToString + ' - ' + DeviceNumber.ToString);
  writedebug('');
  try
    //EMAILHEXMARKER
    for ligne in Datas do
    begin
      writedebug('DeviceMailExtract - Counter ' + counter.ToString + ': ligne - ' + ligne);
      if (counter = DeviceIndex) then
      begin
        writedebug('DeviceMailExtract - Counti ' + counti.tostring + ' IS MATCHING DeviceIndex ' + DeviceIndex.ToString);
        for DeviceLignes in Datas do
        begin
          if (counti > counter) then
          begin
            writedebug('DeviceMailExtract - counter > counti');
            if (DeviceLignes = EMAILHEXMARKER) then
            begin
              writedebug('!!!!! DeviceMailExtract - Counti ' + counti.tostring + ' - DeviceIndex: ' +
                DeviceIndex.ToString + ' MATCHING ' + EMAILHEXMARKER + ' - ' + DeviceNumber.ToString);
              mailadr.Append(StripNonAscii(HexToString(Datas[counti + 1])));
              break;
            end;
          end;
          Inc(counti);
        end;
      end;
      Inc(counter);
    end;
    writedebug('DeviceMailExtract - longueure de mailadr: ' + mailadr.Count.ToString);
    for chainedebug in mailadr do
    begin
      writedebug('DeviceMailExtract - contenu de mailadr: ' + chainedebug);
    end;
    writedebug('DeviceMailExtract - result: ' + mailadr[DeviceNumber]);
  except
    on E: Exception do
    begin
      Writeln('DeviceMailExtract: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;

  Result := mailadr;
end;

function DeviceMailExtractBAD(DataList: TStringList; DeviceIndex: integer): string;
var
  DataLigneDME: string;
  counti: integer = 0;
  DeviceAdminIdentifierAdr: integer;
  mailadr: string = '';
  FULLMAILOFFSET: integer;
begin
  MAILOFFSET := PUREMAILOFFSET;
  if MAILOFFSET = PUREMAILOFFSET then
  begin
    FULLMAILOFFSET := DeviceIndex + MAILOFFSET;
  end
  else
  begin
    FULLMAILOFFSET := DeviceAdminIdentifierAdr + MAILOFFSET;
  end;

  writedebug('Arguments fournis:' + DataList.ToString + ' - ' + DeviceIndex.ToString);
  // Pour chaque ligne du tableau de données
  for DataLigneDME in DataList do
  begin
    writedebug1(counti.tostring + ' - DeviceIndex: ' + DeviceIndex.ToString);
    writedebug1(counti.tostring + ' - DeviceIndex+DEVADMOFFSET: ' + (DeviceIndex + DEVADMOFFSET).ToString);
    writedebug1(counti.tostring + ' - DEVICEADMINIDENTIFIER: ' + DEVICEADMINIDENTIFIER);
    writedebug1(counti.tostring + ' - MAILOFFSET: ' + MAILOFFSET.ToString);
    // Si On arrive sur une ligne indiquée comme début de device
    // et que DEVADMOFFSET lignes plus loin on rencontre bien un identifieur DEVICEADMINIDENTIFIER
    // et que la position actuelle permette d'avance de MAILOFFSET lignes sans dépassement de tableau
    // alors

    if (counti = DeviceIndex) then
    begin
      writedebug(counti.tostring + ' - DeviceIndex: ' + DeviceIndex.ToString + ' - DeviceIndex+DEVADMOFFSET: ' +
        (DeviceIndex + DEVADMOFFSET).ToString + '-' + DataList[DeviceIndex + DEVADMOFFSET] + ' - DEVICEADMINIDENTIFIER: ' +
        DEVICEADMINIDENTIFIER + ' - MAILOFFSET: ' + MAILOFFSET.ToString + '-' + DataList[MAILOFFSET]);
      if (DataList[counti + DEVADMOFFSET] = DEVICEADMINIDENTIFIER) and (DataList.Count > counti + MAILOFFSET) then
      begin
        DeviceAdminIdentifierAdr := counti + DEVADMOFFSET;
        //DeviceAdminIdentifierAdr:= counti;
        // L'adresse mail se trouve en position actuelle + MAILOFFSET
        mailadr := DataList[FULLMAILOFFSET];
        writedebug('DeviceMailExtract - DataLigneDME=' + DataLigneDME);
        writedebug('DeviceMailExtract - DATAMAILadr=' + FULLMAILOFFSET.ToString + '=' + (DeviceAdminIdentifierAdr + MAILOFFSET).ToString);
        writedebug(counti.tostring + ' - DeviceIndex: ' + DeviceIndex.ToString);
        writedebug(counti.tostring + ' - AdmOffset: ' + DEVADMOFFSET.ToString);
        writedebug(counti.tostring + ' - counti+MailOffset:' + (counti + MAILOFFSET).ToString);
        writedebug(counti.tostring + ' - mailadr:' + mailadr);
      end;
    end;
    counti := counti + 1;
  end;

  begin
    for DataLigneDME in DataList do
    begin
      if (DataLigneDME = DEVICEADMINIDENTIFIER) then
        writedebug2('DeviceMailExtract - DataLigneDME=' + DataLigneDME);
    end;
  end;

  Result := mailadr;
end;

function DeviceDataExtract(DeviceStartList: TIntegerArray; TableDeTravail: TStringList): TStringList;
var
  DeviceStart: integer;
  //DeviceStartList: TIntegerArray;
  countk: integer;
  DeviceInfos: string;
  SEP: string;
  SERIAL: integer;
  DEVNAME: integer;
  PRODNAME: integer;
  DevicesInfos: TStringList;
begin
  try
    SEP := SEPARATOR;
    DevicesInfos := TStringList.Create;
    writedebug1('Nombre de marqueurs dans la liste:' + Length(DeviceStartList).ToString);
    countk := 0;
    for DeviceStart in DeviceStartList do
    begin
      SERIAL := DeviceStart + SERIALOFFSET;
      DEVNAME := DeviceStart + DEVNAMEOFFSET;
      PRODNAME := DeviceStart + PRODNAMEOFFSET;
      DeviceInfos := striplastchar(TableDeTravail[SERIAL]) + SEP + striplastchar(TableDeTravail[DEVNAME]) + SEP +
        striplastchar(TableDeTravail[PRODNAME]);
      DevicesInfos.Append(DeviceInfos);
      writedebug1('################################### - ' + countk.tostring + ' - ###################################');
      writedebug1('INDEX   : ' + countk.ToString);
      writedebug1('LIGNE   : ' + DeviceStart.ToString);
      writedebug1('DEVICE  : ' + TableDeTravail[DeviceStart]);
      writedebug1('DEV NAME: ' + TableDeTravail[DeviceStart + 2]);
      writedebug1('PRO NAME: ' + TableDeTravail[DeviceStart + 4]);
      writedebug1('################################################################################');
      countk := countk + 1;
    end;
    Result := DevicesInfos;
  except
    on E: Exception do
    begin
      Writeln('DeviceDataExtract: Une erreur s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;

end;

// Fonction inutilisée, utilisant mon ancienne methode basée sur le bornage par numéro de série.
// Présente un défaut majeur a cause du nombre inconnu de lignes séparant les bornes de
// certaines valeurs, a cause de l'insertion aléatoire selon les fichiers ESS d'un marqueur
// supplémentaire 'FF', entrainant un split d'une chaine de char et générant des lignes non prévisibles.
function DevicesIndexeBySerial(DataList: TStringList): TIntegerArray;
var
  DevicePosition: integer;
  DeviceSerial: string;
  countj: integer;
  endedDevice: boolean;
  offsetA: integer;
  offsetB: integer;
  offsetC: integer;
  DataLigne: string;
  lastStopDevice: integer;
  DeviceCountAndPositions: TIntegerArray;
begin
  try
    offsetA := 4;
    offsetB := 8;
    offsetC := 7;
    endedDevice := False;
    DevicePosition := -1;
    SetLength(DeviceCountAndPositions, 0);
    countj := 0;
    for DataLigne in DataList do
    begin

      // Si La ligne actuelle n'est pas CEmbeddedRDSlF et que la fin d'un device précédent n'est pas signalée
      // alors c'est que c'est le premier device qui commence.
      if (DataLigne = EMBEDEDRDSIDENTIFIER) and not endedDevice then
      begin
        // On ajoute donc une premiere entrée a notre liste de pointeurs de positions de devices.
        SetLength(DeviceCountAndPositions, Length(DeviceCountAndPositions) + 1);
        DevicePosition := countj;
        DeviceCountAndPositions[Length(DeviceCountAndPositions) - 1] := countj + offsetA;
        DeviceSerial := striplastchar(DataList[DevicePosition + offsetA]);
        writedebug('Index serial: ' + Length(DeviceCountAndPositions).ToString + ' - A DeviceSerial = ' + DeviceSerial);
      end;

      // Methode fin par numero de serie - ne permet pas une reprise fiable sur le suivant a cause de byte aléatoires
      // potentiellement a SEPARATORVALUESIDENTIFIER qui ajoutent une ligne de données selon le fichier.
      if DataLigne = DeviceSerial + '@ ' then
      begin
        writedebug('STOP DEVICE: ' + countj.ToString + '-' + DeviceSerial);
        lastStopDevice := countj;
        endedDevice := True;

      end;

      // Si on ne dépasse pas la capacité de notre tableau en travaillant a +offsetB en aval, alors on peut y aller:
      if countj + offsetB < DataList.Count then
      begin
        //writedebug( countj.ToString + ' - ' + DataList.Count.ToString );
        // On veut récupérer le +offsetB apres le stop device si le +offsetC n'est pas un CStockLocation
        writedebug(lastStopDevice);
        writedebug(lastStopDevice + offsetC);
        writedebug(DataList[lastStopDevice + offsetC]);
        if (countj = lastStopDevice) and (endedDevice = True) and not ((DataList[lastStopDevice + offsetC] =
          CSTOCKLOCATIONIDENTIFIER) or (DataList[lastStopDevice + offsetC - 1] = CSTOCKLOCATIONIDENTIFIER)) then
          if (countj = lastStopDevice) and (endedDevice = True) then
          begin
            SetLength(DeviceCountAndPositions, Length(DeviceCountAndPositions) + 1);
            DevicePosition := countj;
            if DataList[DevicePosition + offsetB] <> '' then
              DeviceSerial := striplastchar(DataList[DevicePosition + offsetB]);
            if DataList[DevicePosition + offsetB] = '' then
              DeviceSerial := striplastchar(DataList[DevicePosition + offsetB + 1]);
            DeviceCountAndPositions[length(DeviceCountAndPositions) - 1] := countj + offsetB;
            endedDevice := False;
          end
          else
          begin
            writedebug1('On y vas pas car: countj=' + countj.ToString + '; lastStopDevice=' + lastStopDevice.ToString +
              '; endedDevice=' + endedDevice.ToString() + '; DataList[lastStopDevice + offsetC]=' +
              DataList[lastStopDevice + offsetC] + '; DataList[lastStopDevice + offsetC-1]=' + DataList[lastStopDevice + offsetC - 1] + ';');
          end;
      end;
      countj := countj + 1;
    end;
  except
    on E: Exception do
    begin
      writedebug('Une erreur de liste s''est produite. Détails: ' + E.ClassName + '/' + E.Message);
    end;
  end;
  Result := DeviceCountAndPositions;
end;

procedure writedebug(Chaine: string);
begin
  if DEBUG >= 1 then
    writeln('DEBUG 1:' + Chaine);
end;

procedure writedebug1(Chaine: string);
begin
  if DEBUG >= 2 then
    writeln('DEBUG 2:' + Chaine);
end;

procedure writedebug2(Chaine: string);
begin
  if DEBUG = 3 then
    writeln('DEBUG 3:' + Chaine);
end;

procedure writedebug(Chaine: integer);
begin
  writedebug(Chaine);
end;

procedure writedebug1(Chaine: integer);
begin
  writedebug1(Chaine);
end;

procedure writedebug2(Chaine: integer);
begin
  writedebug2(Chaine);
end;

procedure writelntabledebug(Datalist: TStringList);
var
  countk: integer;
  Dataligne: string;
begin
  countk := 0;
  for DataLigne in Datalist do
  begin
    writedebug(countk.ToString + '-' + DataLigne + '-');
    countk := countk + 1;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  positionnow: integer;
  IndexList: TStringList;
  MyFile: file of byte;
  Data: byte;
  Index: string;
  SEPA: boolean = False;
  SEPB: boolean = False;
  SEPC: boolean = False;
  counti: integer = 0;
  indexcount: integer;
  DataLines: string;
  counter: integer;
begin
  if ParamCount = 0 then
  begin
    writeln(USAGE);
    Application.Terminate;
    exit;
  end;
  if ParamCount = 1 then
  begin
    // Effectuer test d'usage sur le paramètre (extension, ...)
    FICHIER := ParamStr(1);
  end;
  if ParamCount = 2 then
  begin
    // Effectuer test d'usage sur le paramètre (extension, ...)
    FICHIER := ParamStr(1);
    SEPARATOR := ParamStr(2);
  end;
  if ParamCount = 3 then
  begin
    // Effectuer test d'usage sur le paramètre (extension, ...)
    FICHIER := ParamStr(1);
    SEPARATOR := ParamStr(2);
    try
      DEBUG := ParamStr(3).ToInteger;
    except
      on E: Exception do
        writeln('Erreure de format. Le niveau de debug doit être un chiffre entier entre 1 et 3.');
    end;
  end;
  if ParamCount = 4 then
  begin
    // Effectuer test d'usage sur le paramètre (extension, ...)
    FICHIER := ParamStr(1);
    SEPARATOR := ParamStr(2);
    try
      DEBUG := ParamStr(3).ToInteger;
    except
      on E: Exception do
        writeln('Erreure de format. Le niveau de debug doit être un chiffre entier entre 1 et 3.');
    end;
    NOSTRIP := ParamStr(4).ToBoolean;
  end;

  IndexList := TStringList.Create;
  DataValues := TStringList.Create;
  DataValues.Append('');
  AssignFile(MyFile, FICHIER);
    {$I+}
  try
    Reset(MyFile);
    while not EOF(MyFile) do
    begin
      Read(MyFile, Data);
      // Separation des données par le motif FFFEFF en hexadecimal
      if ((Data.ToHexString = SEPARATORVALUESIDENTIFIER) and SEPA and SEPB and not (SEPC)) then
      begin
        SEPC := True;
        writedebug1('FF trouvé ' + FilePos(MyFile).tostring);
      end;
      if ((Data.ToHexString = SEPARATORVALUESIDENTIFIER2) and SEPA and not (SEPB and SEPC)) then
      begin
        SEPB := True;
        writedebug1('FE trouvé ' + FilePos(MyFile).tostring);
      end;
      if ((Data.ToHexString = SEPARATORVALUESIDENTIFIER) and not (SEPA and SEPB and SEPC)) then
      begin
        SEPA := True;
        writedebug1('FF trouvé: ' + FilePos(MyFile).tostring);
        IndexList.Append(FilePos(MyFile).tostring);
      end;
      if SEPA and SEPB and SEPC then
      begin
        SEPA := False;
        SEPB := False;
        SEPC := False;
      end;
    end;
    for Index in IndexList do
    begin
      writedebug2('Index ' + counti.ToString + ': ' + IndexList[counti]);
      counti := counti + 1;
    end;

    // Compteur de numéro d'index
    //counti := 0;
    indexcount := 0;
    Reset(MyFile);
    while not EOF(MyFile) do
    begin
      for Index in IndexList do
      begin
        writedebug1('Seek to: ' + Index);
        // Positionnement de la lecture du fichier a la position voulue
        Seek(MyFile, Index.ToInteger);
        // Pour toutes les position avant le prochain index
        if indexcount + 1 <= IndexList.Count - 1 then
        begin
          try
            for positionnow := Index.ToInteger to IndexList[indexcount + 1].ToInteger do
            begin
              try
                // Lecture du fichier d'un Byte
                Read(MyFile, Data);
                // Ajout de ce Byte dans la ligne concernée par la position actuelle;
                writedebug1('Insert data: "' + Data.ToHexString + '" in position indexcount: ' + indexcount.ToString);
                DataValues[indexcount] := DataValues[indexcount] + Data.ToHexString;
              except
                on E: Exception do
                  writeln('erreure de lecture de données' + E.ClassName + '/' + E.Message);
              end;
            end;
            // Quand toutes les positions on été parcourues jusqu'au prochain index, on change d'index.
            indexcount := indexcount + 1;
            DataValues.Append('');
          except
            on E: Exception do
              writeln('erreure de liste d''index' + E.ClassName + '/' + E.Message);
          end;
        end;
      end;
      exit;
    end;
    // numéro d'index suivant
    //counti := counti + 1;
    //DataValues.Append( '' );
  finally
    ASCIIValues := TStringList.Create;
    for DataLines in DataValues do
    begin
      writedebug1(counter.ToString + ' - "' + StripNonAscii(HexToString(DataLines)) + '"');
      counter := counter + 1;
      ASCIIValues.Append(StripNonAscii(HexToString(DataLines)));
    end;
    CloseFile(MyFile);
  end;
end;

end.
